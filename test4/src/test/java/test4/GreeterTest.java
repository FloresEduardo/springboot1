package test4;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class GreeterTest {
	
	@Test
	public void test()
	{
		Greeter greeter = new Greeter();
		String respuesta = greeter.sayHello();
		String esperado = "Hello world!";
		assertEquals(respuesta, esperado);
	}

}
