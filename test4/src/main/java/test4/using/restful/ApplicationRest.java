package test4.using.restful;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "test4.using.restful")
public class ApplicationRest {
	
	public static void main(String[] args) {
        SpringApplication.run(ApplicationRest.class, args);
    }

}
