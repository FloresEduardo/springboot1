package test4.using.jpa_rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApplicationJpaRest {
	
	public static void main(String[] args) {
		SpringApplication.run(ApplicationJpaRest.class, args);
	}

}
