package test4.using.producing.soap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApplicationProducingSoap {
	
	public static void main(String[] args) {
		SpringApplication.run(ApplicationProducingSoap.class, args);
	}

}
