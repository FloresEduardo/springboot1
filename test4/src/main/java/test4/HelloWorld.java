package test4;

import org.joda.time.LocalTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HelloWorld {
	
	private static final Logger log = LoggerFactory.getLogger(HelloWorld.class);
	
	public static void main(String[] args) {
	
		LocalTime currentTime = new LocalTime();
		log.info("The current local time is: " + currentTime);

		Greeter greeter = new Greeter();
		log.info(greeter.sayHello());
	}
}
